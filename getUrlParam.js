  function getUrlParam(key) {
        var curUrl = window.location.href,
            queryString = curUrl.split('#')[0].split('?')[1] + '&' + curUrl.split('#')[1],
            paramValue = '';
        if (queryString) {
            queryString = queryString.split('&');
            for (var i = 0, len = queryString.length, param; i < len; i++) {
                param = queryString[i].split('=');
                if (decodeURIComponent(param[0]) == key) {
                    paramValue = param[1] ? decodeURIComponent(param[1]) : '';
                    break;
                }
            }
        }
        return paramValue;
    }
    // 来源分析
    function sourceAnalysis() {
        var from, referrer = document.referrer, zcFrom = getUrlParam('zcFrom') || ''; // 众创分享来源标识 wxShare(微信分享) mqqShare(手Q分享)
    	if (!zcFrom) {
			return '';
		}

        // 微信
        if (zcFrom.toLowerCase() == 'wxshare') {
            from = getUrlParam('from'); // 微信来源 groupmessage群组 timeline朋友圈 singlemessage私信
            return zcFrom + '.' + from;
        }

        // 公众号
        if (referrer.indexOf('__biz') >= 0) {
            return 'biz.' + getUrlParam('__biz');
        }

        // 手Q或空间(从手W分享到空间)
        if (zcFrom.toLowerCase() == 'mqqshare') { // 可能来自手Q或QQ空间，需要分析referer
            if (referrer.indexOf('qzone') >= 0) {
                zcFrom = 'qzone';
            }
            return zcFrom; // 返回 mqqshare 或 qzone
        }

        // QQ空间
        if (referrer.indexOf('qzone') >= 0) {
            return 'qzone.' + zcFrom;
        }

        // 微博
        if (referrer.indexOf('weibo.cn') >= 0) {
            return 'weibo.' + zcFrom;
        }

        return zcFrom || '';
    }
	var zcFrom = sourceAnalysis();
	if(zcFrom) {
		document.cookie = 'zcFrom=' + escape(zcFrom);
	}
	var zctag = getUrlParam('zctag');
	if (zctag) {
		var date = new Date();
		var ms = 30 * 24 * 3600 * 1000;
		date.setTime(date.getTime() + ms);
		document.cookie = 'zctag=' + zctag + ';expires=' + date.toGMTString() + ';path=/';
	}