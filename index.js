var stateAndCity = require('./stateAndCity.js');
var learnLodash = require('./learnLodash.js');
var translate = require('./translate.js');
var removeComments = require('./removeComments.js');

var http = require('http') ;

var toDoMethod = translate;
var server = http.createServer(function(req, res){
	res.writeHeader(200,{
		'Content-Type' : 'text/plain;charset=utf-8'
	}) ;
	res.end(JSON.stringify(removeComments(), ' ', ' '));
}) ;
server.listen(8000) ;
console.log("http server running on 127.0.0.1 port 8000") ;
