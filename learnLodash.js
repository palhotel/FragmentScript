var _ = require('lodash');
var data = {
	numbers : ['1', '2', '3', '4', '5', '6'],
	objects : [{name : 'Amy', age : 17}, {name : 'Bob', age : 18}, {name : 'Car', age : 19}],
	functions : {play : function(){console.log('play');}, sleep : function(){console.log('sleep');}},
	mixed : {number : 1, foo : function(){console.log('foo');}, obj : {name : 'xin', age : 20}, array : [1,3,5]},
	zipped : [['fred', 30, true], ['barney', 40, false]]
};

function searchBob(datas) {
	var index = _.findIndex(datas, {name : 'Bob'});
	return datas[index];
}

function pullArray(datas) {
	return _.pull(datas, '3');
}

function removeOdd(datas) {
	var removed = _.remove(datas, function(n){
		return n%2 === 1;
	});
	return {
		src : datas,
		removed : removed
	}
}

function unzipped(datas) {
	return _.unzip(datas);
}

module.exports = function(){
	return {
		datas : data,
		searchBob : searchBob(_.cloneDeep(data.objects)),
		pullArrayWith3 : pullArray(_.cloneDeep(data.numbers)),
		removeOdd : removeOdd(_.cloneDeep(data.numbers)),
		unzipped : unzipped(_.cloneDeep(data.zipped))
	};
};
