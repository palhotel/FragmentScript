/*
remove comments
 */
var sourceCode = '//helloworld\n' +
    '#include<stdio.h>\n' +
    'void main(){\n' +
    'printf("hello,wold\n");\n' +
    '/*some comments here\n' +
    'new line\n' +
    '*/\n' +
    'char* str = "//coments here will not be remove "\n' +
    'printf("%s", str);\n' +
    '}\n';
function removeComments(source) {
   if(source.length > 0) {
       var arr = source.split("");
       var index = -1;
       var isOneline = false;
       var isString = false;
       var isMultLine = false;
       for (var i = 0; i < arr.length; i++) {

           if(!isOneline && !isMultLine) {
               if(arr[i]==='\'' || arr[i] === '"') {
                   isString = true;
               } else if(isString && arr[i]==='"' || arr[i]==='\''){
                   isString = false;
               }
           }
           if(isMultLine && i+1 < arr.length && arr[i] === '*' && arr[i+1] === '/') {
               isMultLine = false;
           } else if (!isMultLine && !isOneline && i+1 < arr.length && arr[i] === '/' && arr[i+1] === '*') {
               isMultLine = true;
           } else if(isOneline && i+1 < arr.length && arr[i+1] ==='\n') {
               isOneline = false;
           } else if (!isString && !isMultLine && arr[i] === '/' && i+1 < arr.length && arr[i+1] === '/') {
               isOneline = true;
           } else if(!isOneline && !isMultLine){
               index = index + 1;
               arr[index] = arr[i];
           }
       }
       arr.length = index + 1;
       return arr.join('');
   } else {
       return 0;
   }

}
module.exports = function(){
    return {
        source : sourceCode,
        removed : removeComments(sourceCode)
    };
};
